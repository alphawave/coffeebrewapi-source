<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Coffee;
use App\Models\Drink;
use App\Models\Ingredient;
use App\Models\Machine;
use App\Models\Milk;
use App\Models\Water;
use Illuminate\Database\Seeder;

class DrinksSeeder extends Seeder {
    public function run(): void {
//Эспрессо	Кофе	7 гр	Тонкий помол
//Эспрессо	Вода	30 мл	90°C
//Эспрессо	Кофемашина	25 сек	9 бар
//        $drink = Drink::create([
//            'name' => 'espresso'
//        ]);
//        $drink->coffees()->create([
//            'weight' => 7,
//            'grind' => 'fine'
//        ]);
//        $drink->waters()->create([
//            'quantity' => 30,
//            'temperature' => 90
//        ]);
//        $drink->machines()->create([
//            'duration' => 30
//        ]);

        Drink::create([
            'name' => 'espresso'
        ])->ingredients()->saveMany([
            Coffee::create([
                'weight' => 7,
                'grind' => 'fine'
            ])->ingredient()->create(),
            Water::create([
                'quantity' => 30,
                'temperature' => 90
            ])->ingredient()->create(),
            Machine::create([
                'duration' => 30
            ])->ingredient()->create(),
        ]);


//Ристретто	Кофе	7 гр	Тонкий помол
//Ристретто	Вода	15 мл	90°C
//Ристретто	Кофемашина	15 сек	9 бар
        $drink = Drink::create([
            'name' => 'ristretto'
        ]);
        $drink->coffees()->create([
            'weight' => 7,
            'grind' => 'fine'
        ]);
        $drink->waters()->create([
            'quantity' => 15,
            'temperature' => 90
        ]);
        $drink->machines()->create([
            'duration' => 30
        ]);

//        Drink::create([
//            'name' => 'ristretto'
//        ])->ingredients()->saveMany([
//            Coffee::create([
//                'weight' => 7,
//                'grind' => 'fine'
//            ])->ingredient()->create(),
//            Water::create([
//                'quantity' => 15,
//                'temperature' => 90
//            ])->ingredient()->create(),
//            Machine::create([
//                'duration' => 30
//            ])->ingredient()->create(),
//        ]);

//Доппио	Кофе	14 гр	Тонкий помол
//Доппио	Вода	60 мл	90°C
//Доппио	Кофемашина	25 сек	9 бар
//        $drink = Drink::create([
//            'name' => 'doppio'
//        ]);
//        $drink->coffees()->create([
//            'weight' => 14,
//            'grind' => 'fine'
//        ]);
//        $drink->waters()->create([
//            'quantity' => 60,
//            'temperature' => 90
//        ]);
//        $drink->machines()->create(['duration' => 25]);

        Drink::create([
            'name' => 'doppio'
        ])->ingredients()->saveMany([
            Coffee::create([
                'weight' => 14,
                'grind' => 'fine'
            ])->ingredient()->create(),
            Water::create([
                'quantity' => 60,
                'temperature' => 90
            ])->ingredient()->create(),
            Machine::create([
                'duration' => 25
            ])->ingredient()->create(),
        ]);

//Триппло	Кофе	21 гр	Тонкий помол
//Триппло	Вода	90 мл	90°C
//Триппло	Кофемашина	25 сек	9 бар
//        $drink = Drink::create(['name' => 'tripplo']);
//        $drink->coffees()->create([
//            'weight' => 21,
//            'grind' => 'fine'
//        ]);
//        $drink->waters()->create([
//            'quantity' => 90,
//            'temperature' => 90
//        ]);
//        $drink->machines()->create(['duration' => 25]);

        Drink::create([
            'name' => 'tripplo'
        ])->ingredients()->saveMany([
            Coffee::create([
                'weight' => 21,
                'grind' => 'fine'
            ])->ingredient()->create(),
            Water::create([
                'quantity' => 90,
                'temperature' => 90
            ])->ingredient()->create(),
            Machine::create([
                'duration' => 25
            ])->ingredient()->create(),
        ]);


//Лунго	Кофе	7 гр	Тонкий помол
//Лунго	Вода	50 мл	90°C
//Лунго	Кофемашина	20 сек	9 бар
//        $drink = Drink::create(['name' => 'lungo']);
//        $drink->coffees()->create([
//            'weight' => 7,
//            'grind' => 'fine'
//        ]);
//        $drink->waters()->create([
//            'quantity' => 50,
//            'temperature' => 90
//        ]);
//        $drink->machines()->create(['duration' => 20]);

        Drink::create([
            'name' => 'lungo'
        ])->ingredients()->saveMany([
            Coffee::create([
                'weight' => 7,
                'grind' => 'fine'
            ])->ingredient()->create(),
            Water::create([
                'quantity' => 50,
                'temperature' => 90
            ])->ingredient()->create(),
            Machine::create([
                'duration' => 20
            ])->ingredient()->create(),
        ]);


//Американо	Кофе	7 гр	Тонкий помол
//Американо	Вода	90 мл	90°C
//Американо	Кофемашина	20 сек	9 бар
//        $drink = Drink::create(['name' => 'americano']);
//        $drink->coffees()->create([
//            'weight' => 7,
//            'grind' => 'fine'
//        ]);
//        $drink->waters()->create([
//            'quantity' => 90,
//            'temperature' => 90
//        ]);
//        $drink->machines()->create(['duration' => 20]);

        Drink::create([
            'name' => 'americano'
        ])->ingredients()->saveMany([
            Coffee::create([
                'weight' => 7,
                'grind' => 'fine'
            ])->ingredient()->create(),
            Water::create([
                'quantity' => 90,
                'temperature' => 90
            ])->ingredient()->create(),
            Machine::create([
                'duration' => 20
            ])->ingredient()->create(),
        ]);

//Капучино	Кофе	14 гр	Тонкий помол
//Капучино	Вода	60 мл	90°C
//Капучино	Молоко	60 мл	60°C
//Капучино	Молочная пена	60 мл	60°C
//Капучино	Кофемашина	20 сек	9 бар
//        $drink = Drink::create(['name' => 'cappuccino']);
//        $drink->coffees()->create([
//            'weight' => 14,
//            'grind' => 'fine'
//        ]);
//        $drink->waters()->create([
//            'quantity' => 60,
//            'temperature' => 90
//        ]);
//        $drink->milks()->create([
//            'quantity' => 60,
//            'temperature' => 60
//        ]);
//        $drink->machines()->create(['duration' => 20]);

        Drink::create([
            'name' => 'cappuccino'
        ])->ingredients()->saveMany([
            Coffee::create([
                'weight' => 14,
                'grind' => 'fine'
            ])->ingredient()->create(),
            Water::create([
                'quantity' => 60,
                'temperature' => 90
            ])->ingredient()->create(),
            Milk::create([
                'quantity' => 60,
                'temperature' => 60
            ])->ingredient()->create(),
            Machine::create([
                'duration' => 20
            ])->ingredient()->create(),
        ]);
    }
}
