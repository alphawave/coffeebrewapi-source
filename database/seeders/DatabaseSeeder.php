<?php
declare(strict_types=1);

use Database\Seeders\DrinksSeeder;
use Database\Seeders\UsersSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    public function run(): void {
        $this->call(DrinksSeeder::class);
        $this->call(UsersSeeder::class);
    }
}
