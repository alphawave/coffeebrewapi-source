<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrinksTable extends Migration {
    public function up(): void {
        Schema::create('drinks', function(Blueprint $table) {
            $table->string('name', 191)->primary();
            $table->text('description')->nullable();
//            $table->string('ingredient_type', 191)->nullable();
//            $table->unsignedTinyInteger('ingredient_id')->nullable();
//            $table->index([
//                'ingredient_type',
//                'ingredient_id'
//            ]);
            $table->timestamps();
        });
    }


    public function down(): void {
        Schema::dropIfExists('drinks');
    }
}
