<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoffeesTable extends Migration {
    public function up(): void {
        Schema::create('coffees', function(Blueprint $table) {
            $table->unsignedTinyInteger('id', true);
            $table->unsignedSmallInteger('weight');
            $table->enum('grind', ['fine', 'medium', 'coarse']);
        });
    }


    public function down(): void {
        Schema::dropIfExists('coffees');
    }
}
