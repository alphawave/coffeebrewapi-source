<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWatersTable extends Migration {
    public function up(): void {
        Schema::create('waters', function(Blueprint $table) {
            $table->unsignedTinyInteger('id', true);
            $table->unsignedSmallInteger('quantity');
            $table->unsignedTinyInteger('temperature');
        });
    }


    public function down(): void {
        Schema::dropIfExists('waters');
    }
}
