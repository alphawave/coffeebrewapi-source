<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsTable extends Migration {
    public function up(): void {
        Schema::create('ingredients', function(Blueprint $table) {
            $table->id();
            $table->string('drink_name', 191)->nullable()->index();
            $table->string('ingredientable_type', 191)->nullable();
            $table->unsignedTinyInteger('ingredientable_id')->nullable();
            $table->index([
                'ingredientable_type',
                'ingredientable_id'
            ]);
        });
    }


    public function down(): void {
        Schema::dropIfExists('ingredients');
    }
}
