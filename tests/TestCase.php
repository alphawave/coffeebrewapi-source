<?php
declare(strict_types=1);

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase {
    public function createApplication() {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
