<?php
declare(strict_types=1);

class WebTest extends TestCase {
    public function testDrinksList(): void {
        $this->get('/')->seeJson(['name' => 'espresso']);
    }


//    public function testSomeRecipe(): void {
//        $this->get('/espresso')->seeJson([
//            'coffees' => [
//                [
//                    'grind' => 'fine',
//                    'weight' => 7
//                ]
//            ],
//            'machines' => [
//                [
//                    'duration' => 30
//                ]
//            ],
//            'milks' => [],
//            'name' => 'espresso',
//            'waters' => [
//                [
//                    'quantity' => 30,
//                    'temperature' => 90
//                ]
//            ]
//        ]);
//    }
    public function testEspressoRecipe(): void {
        $this->get('/espresso')->seeJson([
            'data' => [
                'name' => 'espresso',
                'description' => null,
                'ingredients' => [
                    [
                        'ingredientable' => [
                            'weight' => 7,
                            'grind' => 'fine'
                        ],
                        'ingredientable_type' => 'Coffee'
                    ],
                    [
                        'ingredientable' => [
                            'quantity' => 30,
                            'temperature' => 90
                        ],
                        'ingredientable_type' => 'Water'
                    ],
                    [
                        'ingredientable' => [
                            'duration' => 30
                        ],
                        'ingredientable_type' => 'Machine'
                    ]
                ]
            ]
        ]);
    }

    public function testRandomRecipe(): void {
        $this->get('/random')->seeJsonStructure([
            'data' => [
                'name',
                'description',
                'ingredients'
            ],
        ]);
    }


    public function testNonExistRecipe(): void {
        $this->get('/non-exist')->seeJson(['message' => 'Not Found!'])->seeStatusCode(404);
    }
}
