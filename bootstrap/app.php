<?php
declare(strict_types=1);

use App\Exceptions\Handler;
use App\Http\Middleware\Authenticate;
use App\Providers\AuthServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Laravel\Lumen\Application;
use Laravel\Lumen\Bootstrap\LoadEnvironmentVariables;
use Laravel\Lumen\Routing\Router;

require_once __DIR__.'/../vendor/autoload.php';

(new LoadEnvironmentVariables(dirname(__DIR__)))->bootstrap();

date_default_timezone_set(env('APP_TIMEZONE', 'UTC'));

$app = new Application(dirname(__DIR__));


//$app->withFacades();
$app->withEloquent();


$app->singleton(ExceptionHandler::class, Handler::class);
$app->singleton(Kernel::class, \App\Console\Kernel::class);


$app->configure('app');


$app->routeMiddleware([
    'auth' => Authenticate::class,
]);


// $app->register(App\Providers\AppServiceProvider::class);
$app->register(AuthServiceProvider::class);
if($app->environment() !== 'production') {
    $app->register(IdeHelperServiceProvider::class);
}


$app->router->group(['namespace' => '\App\Http\Controllers',], static function(Router $router): void {
    require __DIR__.'/../routes/api.php';
});

return $app;
