<?php /** @noinspection PhpMissingStrictTypesDeclarationInspection */

use Illuminate\Database\Eloquent\Builder;

return [
    'filename' => '_ide_helper',
    'format' => 'php',
    'meta_filename' => '.phpstorm.meta.php',
    'include_fluent' => true,
    'include_factory_builders' => true,
    'write_model_magic_where' => true,
    'write_eloquent_model_mixins' => true,
    'include_helpers' => true,
    'helper_files' => [],
    'model_locations' => [
        'app/Models',
    ],
    'ignored_models' => [],
    'extra' => [
        'Eloquent' => [
            Builder::class,
            \Illuminate\Database\Query\Builder::class
        ],
    ],
    'magic' => [],
    'interfaces' => [],
    'custom_db_types' => [],
    'model_camel_case_properties' => false,
    'type_overrides' => [
        'integer' => 'int',
        'boolean' => 'bool',
    ],
    'include_class_docblocks' => true,
];
