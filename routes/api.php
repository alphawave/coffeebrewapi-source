<?php
declare(strict_types=1);

/** @var \Laravel\Lumen\Routing\Router $router */


$router->get('/', 'DrinkController@index');
$router->get('/random', 'DrinkController@random');
$router->get('/{drink}', 'DrinkController@show');

$router->post('/register', 'UserController@register');
$router->post('/login', 'UserController@login');
