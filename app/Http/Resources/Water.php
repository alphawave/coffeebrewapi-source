<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

final class Water extends JsonResource {
    public function toArray($request): array {
        return [
            'quantity' => $this->quantity,
            'temperature' => $this->temperature,
        ];
    }
}
