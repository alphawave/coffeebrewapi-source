<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

final class Machine extends JsonResource {
    public function toArray($request): array {
        return [
            'duration' => $this->duration,
        ];
    }
}
