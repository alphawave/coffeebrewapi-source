<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

final class Coffee extends JsonResource {
    public function toArray($request): array {
        return [
            'weight' => $this->weight,
            'grind' => $this->grind,
        ];
    }
}
