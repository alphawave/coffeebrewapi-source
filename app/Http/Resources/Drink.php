<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

final class Drink extends JsonResource {
    public function toArray($request): array {
        return [
            'name' => $this->name,
            'description' => $this->description,
//            'coffees' => Coffee::collection($this->whenLoaded('coffees')),
//            'machines' => Machine::collection($this->whenLoaded('machines')),
//            'milks' => Milk::collection($this->whenLoaded('milks')),
//            'waters' => Water::collection($this->whenLoaded('waters')),
            'ingredients' => Ingredient::collection($this->whenLoaded('ingredients'))
        ];
    }
}
