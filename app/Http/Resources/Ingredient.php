<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use function class_basename;

final class Ingredient extends JsonResource {
    public function toArray($request): array {
        return [
            'ingredientable' => $this->ingredientable->makeHidden(['id']),
            'ingredientable_type' => class_basename($this->ingredientable_type),
        ];
    }
}
