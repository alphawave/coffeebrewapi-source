<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\Drink as DrinkResource;
use App\Models\Drink;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Laravel\Lumen\Routing\Controller;

final class DrinkController extends Controller {
    public function __construct() {
//        $this->middleware('auth');
    }


    public function index(): AnonymousResourceCollection {
        return DrinkResource::collection(Drink::all());
//        return Drink::all();
    }


    public function show(string $drink): DrinkResource {
//        return new DrinkResource(Drink::with('coffees', 'machines', 'milks', 'waters')->findOrFail($recipe));
//        return Drink::with( 'coffees', 'machines', 'milks', 'waters')->findOrFail($recipe);
        return new DrinkResource(Drink::with('ingredients.ingredientable')->findOrFail($drink));
    }


    public function random(): DrinkResource {
        return new DrinkResource(Drink::with('ingredients.ingredientable')->inRandomOrder()->firstOrFail());
    }
}
