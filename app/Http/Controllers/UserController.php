<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Lumen\Routing\Controller;

final class UserController extends Controller {
    public function register(Request $request): JsonResponse {
        $this->validate($request, [
            'name' => 'required',
            'email' => [
                'required',
                'email',
                'unique:users'
            ],
            'password' => 'required'
        ]);

        $api_token = $this->createToken();

        $request->merge([
            'api_token' => $api_token,
            'password' => app('hash')->make($request->input('password'))
        ]);

        User::create($request->all());

        return response()->json(['data' => ['api_token' => $api_token]]);
    }


    public function login(Request $request): JsonResponse {
        $this->validate($request, [
            'email' => [
                'required',
                'email'
            ],
            'password' => 'required'
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if(!$user || !app('hash')->check($request->input('password'), $user->password)) {
            return response()->json(['message' => 'Invalid email or password'], 401);
        }

        $api_token = $this->createToken();

        $user->api_token = $api_token;

        $user->save();

        return response()->json(['data' => ['api_token' => $api_token]]);
    }


    private function createToken(): string {
        return hash('sha256', Str::random(40));
    }
}
