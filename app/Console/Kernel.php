<?php
declare(strict_types=1);

namespace App\Console;

use Laravel\Lumen\Console\Kernel as ConsoleKernel;

final class Kernel extends ConsoleKernel {
}
