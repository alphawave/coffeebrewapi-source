<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

final class Drink extends Model {
    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * @var bool
     */
    public $incrementing = false;


    public function ingredients(): HasMany {
        return $this->hasMany(Ingredient::class);
    }


    public function coffees(): MorphToMany {
//        return $this->morphedByMany(Coffee::class, 'ingredient');
        return $this->morphedByMany(Coffee::class, 'ingredientable', 'ingredients');
    }


    public function machines(): MorphToMany {
//        return $this->morphedByMany(Machine::class, 'ingredient');
        return $this->morphedByMany(Machine::class, 'ingredientable', 'ingredients');
    }


    public function milks(): MorphToMany {
//        return $this->morphedByMany(Milk::class, 'ingredient');
        return $this->morphedByMany(Milk::class, 'ingredientable', 'ingredients');
    }


    public function waters(): MorphToMany {
//        return $this->morphedByMany(Water::class, 'ingredient');
        return $this->morphedByMany(Water::class, 'ingredientable', 'ingredients');
    }
}
