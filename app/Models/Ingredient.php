<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

final class Ingredient extends Model {
    public function usesTimestamps(): bool {
        return false;
    }

    public function drink(): BelongsTo {
        return $this->belongsTo(Drink::class);
    }


    public function ingredientable(): MorphTo {
        return $this->morphTo();
//        return $this->morphTo(__FUNCTION__, 'ingredient_type', 'ingredient_id');
    }
}
