<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

final class Milk extends Model {
    public function usesTimestamps(): bool {
        return false;
    }


    public function ingredient(): MorphOne {
        return $this->morphOne(Ingredient::class, 'ingredientable');
    }


    public function drinks(): MorphToMany {
        return $this->morphToMany(Drink::class, 'ingredient');
    }
}
