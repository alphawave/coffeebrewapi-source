<?php
declare(strict_types=1);

namespace App\Providers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

final class AuthServiceProvider extends ServiceProvider {
    public function boot(): void {
        $this->app['auth']->viaRequest('api', function(Request $request) {
            if($request->bearerToken()) {
                return User::where('api_token', $request->bearerToken())->first();
            }

            return null;
        });
    }
}
